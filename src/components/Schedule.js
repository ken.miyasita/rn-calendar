import React from 'react';
import { Alert, AppRegistry, Button, StyleSheet, View, Text, FlatList } from 'react-native';

const Schedule = (props) => {
  // {props.navi.events.length === 0 ?
  // <EventList events={[]}/> :
  // <EventList events={props.navi.events[props.navi.dayOfMonth]}/>}

  return(
    <View style={styles.container}>
      <Text>Schedule of the Day</Text>
      <FlatList
        data={[
          {key: 'a', text: 'do something'},
          {key: 'b', text: 'do another'},
          {key: 'c', text: 'take a rest'},
        ]}
        renderItem={({item})=><Text style={styles.itemText}>{item.text}</Text>}
      />
    </View>
  );
}

/**
  * @param {Array} events Array of events.
  */
const EventList = (props) => {
  var liArray = [];
  for(var i = 0; i < props.events.length; i++){
    var e = props.events[i];
    var time = '--:--'
    if(e.start.dateTime){     // e.g.) 2018-03-15T00:00:00
      time = e.start.dateTime.substring(11,16);
    }
    liArray.push(<li key={[e.etag, time]}>
      {'[' + time + '] ' + e.summary}</li>);
  }
  return liArray;
}

const styles = StyleSheet.create({
  container: { flex: 20, backgroundColor: '#ffffff' },
  itemText: {
    // padding: 10,
    // fontSize: 18,
    // height: 44
  }

});

export default Schedule;
